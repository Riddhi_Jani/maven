package com.crm.qa.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import com.crm.qa.util.TestUtil;
import com.crm.qa.util.WebEventListener;
import io.github.bonigarcia.wdm.WebDriverManager;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.openqa.selenium.chromium.ChromiumDriver;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.network.Network;
import org.openqa.selenium.devtools.network.model.Headers;







public class TestBase {
	
	public static WebDriver driver;
	public static Properties prop;
	public  static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	
	public TestBase(){
		
		prop = new Properties();
			try {
			
				FileInputStream ip = new FileInputStream("C:\\Users\\manis\\Desktop\\Selenium Project Backup (GITHUB)\\PageObjectModel-master\\src\\main\\java\\com\\crm\\qa\\config\\config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	} 
	
	
	public static void initialization(){
	String browserName = prop.getProperty("browser");
		
	if(browserName.equals("chrome")){
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\manis\\Desktop\\Selenium Webdriver Setup\\chromedriver.exe");	
		driver = new ChromeDriver(); 
		}
		else if(browserName.equals("FF")){
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\manis\\Desktop\\Selenium Webdriver Setup\\geckodriver.exe");	
			driver = new FirefoxDriver(); 
		}
		
		
		//e_driver = new EventFiringWebDriver(driver);
		//Now create object of EventListerHandler to register it with EventFiringWebDriver
		//eventListener = new WebEventListener();
		//e_driver.register(eventListener);
		//driver = e_driver;
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		
		
		
	//	WebDriverManager.chromedriver().setup();
		//driver = new ChromeDriver();
	//	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	//	driver.manage().window().maximize();
	//	String username = "rochportal"; // authentication username
	//	String password = "#P0rt4rOcH@"; // authentication password

		// Get the devtools from the running driver and create a session
	//	DevTools devTools = ((ChromiumDriver) driver).getDevTools();		
	//	devTools.createSession(); 

		// Enable the Network domain of devtools
	//	devTools.send(Network.enable(Optional.of(100000), Optional.of(100000), Optional.of(100000)));
	//	String auth = username +":"+ password;

		// Encoding the username and password using Base64 (java.util)
	//String encodeToString = Base64.getEncoder().encodeToString(auth.getBytes());

		// Pass the network header -> Authorization : Basic <encoded String>
	//	Map<String, Object> headers = new HashMap<String, Object>();
	//	headers.put("Authorization", "Basic "+encodeToString);				
	//	devTools.send(Network.setExtraHTTPHeaders(new Headers(headers)));
		
		driver.get(prop.getProperty("url"));

			
		
		
	}
	
	
	
	
	
	
	
	

}
